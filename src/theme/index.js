import { createTheme } from '@material-ui/core/styles';
import palette from './palette';

const theme = createTheme({
  palette: { ...palette },
});

export default theme;
