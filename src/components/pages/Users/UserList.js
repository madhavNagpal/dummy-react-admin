import React from "react";
import { makeStyles } from "@material-ui/core";
import { List, Datagrid, TextField, EmailField } from "react-admin";
import { useListContext } from "react-admin";

import CustomUrlField from "../../atoms/CustomUrlField";

const useStyles = makeStyles((theme) => ({
  container: {
    display: "grid",
    gridTemplateColumns: "1fr 1fr 1fr",
    [theme.breakpoints.down("md")]: {
      gridTemplateColumns: "1fr 1fr",
    },
    [theme.breakpoints.down("sm")]: {
      gridTemplateColumns: "1fr",
    },
  },
  user: {
    padding: "8px",
    textAlign: "center",
    borderRadius: "5px",
    margin: "16px",
    background: theme.palette.primary.main,
    color: theme.palette.basic.white,
  },
}));

const User = () => {
  const classes = useStyles();
  const { data, loading } = useListContext();
  const users = Object.values(data);
  console.log(data, " data in user");
  if (loading) return null;
  return (
    <div className={classes.container}>
      {users.map(({ id, name, email, website }) => (
        <div key={id} className={classes.user}>
          <h2>{name}</h2>
          <h3>{email}</h3>
        </div>
      ))}
    </div>
  );
};

const UserList = (props) => (
  <List {...props}>
    <>
      <User />
      {/* <Datagrid rowClick="edit">
        <TextField source="id" />
        <TextField source="name" />
        <EmailField source="email" />
        <TextField source="phone" />
        <CustomUrlField source="website" />
        <TextField source="company.name" />
      </Datagrid> */}
    </>
  </List>
);

export default UserList;
