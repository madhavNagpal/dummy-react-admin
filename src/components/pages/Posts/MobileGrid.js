import React from "react";
import { makeStyles } from "@material-ui/core";
import { useListContext } from "react-admin";

const useStyles = makeStyles((theme) => ({
  container: {
    padding: "8px",
    textAlign: "center",
    borderRadius: "5px",
    margin: "16px",
    background: theme.palette.primary.main,
    color: theme.palette.basic.white,
  },
  title: {
    textTransform: "capitalize",
  },
  body: {},
}));

const Post = ({ title, body }) => {
  const classes = useStyles();

  return (
    <div className={classes.container}>
      <h2 className={classes.title}>{title}</h2>
      <p className={classes.body}>{body}</p>
    </div>
  );
};

const MobileGrid = () => {
  const { data, loading } = useListContext();
  const posts = Object.values(data);

  if (loading) return null;

  return (
    <div>
      {posts.map((post) => (
        <Post key={post.id} {...post} />
      ))}
    </div>
  );
};

export default MobileGrid;
