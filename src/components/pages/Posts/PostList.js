import React from "react";
import {
  List,
  // SimpleList,
  Datagrid,
  TextField,
  ReferenceField,
  EditButton,
  ReferenceInput,
  SelectInput,
  TextInput,
} from "react-admin";
import { useMediaQuery } from "@material-ui/core";

import MobileGrid from "./MobileGrid";

const postFilters = [
  <TextInput source="q" label="Search" alwaysOn />,
  <ReferenceInput source="userId" label="User" reference="users" allowEmpty>
    <SelectInput optionText="name" />
  </ReferenceInput>,
];

const PostList = (props) => {
  const isSmall = useMediaQuery((theme) => theme.breakpoints.down("sm"));
  return (
    <List filters={postFilters} {...props}>
      {isSmall ? (
        <>
          <MobileGrid />

          {/* <SimpleList
            primaryText={(record) => record.title}
            secondaryText={(record) => `${record.views} views`}
            tertiaryText={(record) =>
              new Date(record.published_at).toLocaleDateString()
            }
          /> */}
        </>
      ) : (
        <Datagrid>
          <TextField source="id" />
          <ReferenceField label="User" source="userId" reference="users">
            <TextField source="name" />
          </ReferenceField>
          <TextField source="title" />
          <TextField source="body" />
          <EditButton />
        </Datagrid>
      )}
    </List>
  );
};

export default PostList;
